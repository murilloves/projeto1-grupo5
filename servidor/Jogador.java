package servidor;

import java.net.Socket;

public class Jogador {

	/**
	 * @param args
	 */
	private String nome;
	private int chute;
	private int pontuacao;
	private int palitosTotal;
	private int palitosJogados;
	private Socket conexao;
	private Jogo jogo;
	

	public Jogador(String nome, Socket conexao, Jogo jogo){
		this.nome = nome;
		this.chute = 0;
		this.pontuacao = 0;
		this.palitosJogados=0;
		this.palitosTotal = 3;
		this.conexao = conexao;
		this.jogo = jogo;
	}
	
	public Jogo getJogo() {
		return jogo;
	}

	public void setJogo(Jogo jogo) {
		this.jogo = jogo;
	}

	public Socket getConexao() {
		return conexao;
	}

	public void setConexao(Socket conexao) {
		this.conexao = conexao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getChute() {
		return chute;
	}

	public void setChute(int chute) {
		this.chute = chute;
	}

	public int getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}

	public int getPalitosTotal() {
		return palitosTotal;
	}

	public void setPalitosTotal(int palitosTotal) {
		this.palitosTotal = palitosTotal;
	}

	public int getPalitosJogados() {
		return palitosJogados;
	}

	public void setPalitosJogados(int palitosJogados) {
		this.palitosJogados = palitosJogados;
	}
	
	

}
