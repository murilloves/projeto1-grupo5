package servidor;

import java.awt.List;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;


public class ServidorJogo{
	private ServerSocket conexaoServidor;
	private boolean status;
	ArrayList<ClienteThread> clientes = new ArrayList<ClienteThread>();
	
	public ServidorJogo(int porta) {
	      try {
	         this.conexaoServidor = new ServerSocket(porta);
	         this.status = true;
	   		System.out.println("Esperando conexoes na porta " + porta);
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	   }
	
	
	public boolean getStatus() {
		return status;
	}
	


	public ArrayList<ClienteThread> getClientes() {
		return clientes;
	}


	public void setClientes(ArrayList<ClienteThread> clientes) {
		this.clientes = clientes;
	}


	public void setStatus(int status) {
		status = status;
	}
	


	public ServerSocket getConexaoServidor() {
		return conexaoServidor;
	}


	public void setConexaoServidor(ServerSocket conexaoServidor) {
		this.conexaoServidor = conexaoServidor;
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		ServidorJogo servidor = new ServidorJogo(9090);
        System.out.println("Porta 9090 aberta!");      
        System.out.println("Aguando conexao do jogador...");
        
        while(true){
        	if(servidor.getStatus()){
	        	Socket cliente = servidor.getConexaoServidor().accept();
	        	ClienteThread novo = new ClienteThread(cliente);
	        	servidor.getClientes().add(novo); 		
	        	novo.start();
        	}
        }       
	}

}
