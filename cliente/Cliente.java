package cliente;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente implements Runnable{
	
	 private Socket cliente;

	    public Cliente(Socket cliente){
	        this.cliente = cliente;
	    }

	/**
	 * @param args
	 */
	public static void main(String[] args) throws UnknownHostException, IOException {
		// TODO Auto-generated method stub
		
		Socket cliente =  new Socket("localhost", 9090);
		
		Cliente c = new Cliente(cliente);
		Thread t = new Thread(c);
		t.start();
	}
	
	
	public void run(){
		try {
			PrintStream saida;
			System.out.println("O cliente conectou ao servidor");
			
			
			Scanner entrada = new Scanner(System.in);
			
			saida = new PrintStream(this.cliente.getOutputStream());
			
			while(entrada.hasNextLine()){
				saida.println(entrada.nextLine());
			}
			
			saida.close();
			entrada.close();
			this.cliente.close();
			System.out.println("Fim da thread...");
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		
	}

}
